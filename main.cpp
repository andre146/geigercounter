#ifndef F_CPU
#define F_CPU 4000000UL //change the damn timer prescalers if you touch this!
#define F_CPU_F 4000000.0F
#endif

#define __AVR_ATmega328__

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <avr/eeprom.h>
#include <util/delay.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "lcd/lcd.h"
#include "lcd/lcd.c"
#include "uart/uart.h"
#include "uart/uart.c"

#define LED_PORT PORTD
#define LED_PIN PD4
#define TIMER2_REGISTER (1<<CS22)|(1<<CS21)
#define CLOCK_TIME 0.256 
#define ADC_RES 1024.0F
#define RESISTOR_R1 20700000.0F
#define RESISTOR_R2 118500.0F   
#define SET_POINT 390 //the set point in volts
#define PROP_GAIN 10 //proportional gain
#define INT_GAIN 5//integral gain
#define DEV_GAIN 0 //derivative gain
#define DELAY 10 //delay time between loops
#define AVG_CNT 800
#define UART_BAUD_RATE 19200

/*Bit definitions for control bool variable 0*/
#define TICK_CHANGE 0
#define RATE_CHANGE 1
#define BTN_PRESSED 2
#define VOLTAGE_CHANGE 3
#define MENU_CHANGE 4
#define ADC_CALIBRATED 5

/*UART instruction byte definitions, get instructions start at 1*/
#define UART_GET_PGAIN 0x1
#define UART_GET_IGAIN 0x2
#define UART_GET_OUTPUT_VOLTAGE 0x3
#define UART_GET_TARGET_VOLTAGE 0xC
#define UART_GET_CYCLE_TIME 0x4
#define UART_GET_TIME 0x5
#define UART_GET_UART_BAUD_RATE 0x6 //this is a bit debatable...
#define UART_GET_REF_VOLTAGE 0x7
#define UART_GET_RESISTOR_1_VALUE 0x8
#define UART_GET_RESISTOR_2_VALUE 0x9
#define UART_GET_ADC_AVG_COUNTS 0xA
#define UART_GET_TOTAL_COUNTS 0xB

/*UART instruction byte definitions, set instructions start at 127 (0x7F)*/
#define UART_SET_PGAIN 0x7F
#define UART_SET_IGAIN 0x80
#define UART_SET_TARGET_VOLTAGE 0x81
#define UART_SET_UART_BAUD_RATE 0x82 //this is a bit debatable...
#define UART_SET_RESISTOR_1_VALUE 0x83
#define UART_SET_RESISTOR_2_VALUE 0x84
#define UART_SET_ADC_AVG_COUNTS 0x85

/*UART return value definitions*/
#define UART_INVALID_INST 0xff
#define UART_VALID_RETURN 0x1
#define UART_SUCCESSFULL 0x2
#define UART_ERROR_OVERFLOW 0x3
#define UART_ERROR_OVERRUN 0x4
#define UART_ERROR_FRAME 0x5

/*global variables are shared between functions (volatile if used in ISRs)*/
volatile float adcRefVoltage = 5;
volatile float voltsPerDigit = 0;
volatile uint32_t ovf = 0; //number of timer 2 overflows
volatile uint8_t ctrlBools0 = 0; //control bool variable 0
volatile uint8_t menuState = 1;
volatile uint32_t ticks = 0; //number of ticks counted
volatile float ticksPerSecond = 0;
volatile float ticksPerMinute = 0;
volatile float ticksPerHour = 0;
volatile float ticksPerSecondAvg = 0;
volatile float ticksPerMinuteAvg = 0;
volatile float ticksPerHourAvg = 0;
float loopTime = 0; //the cycle time of the main loop
int16_t currVoltage = 0; //the current voltage on the HV side

uint8_t propGain;
uint8_t intGain;
uint16_t setPoint;
float resistor1;
float resistor2;

/*EEPROM variables*/
uint8_t propGainEE EEMEM = PROP_GAIN;
uint8_t intGainEE EEMEM = INT_GAIN;
uint16_t setPointEE EEMEM = SET_POINT;
float resistor1EE EEMEM = RESISTOR_R1;
float resistor2EE EEMEM = RESISTOR_R2;

/*LCD string init*/
const char lcdString0[] PROGMEM = "Geigercounter\nby Andre Picker";
const char lcdString1[] PROGMEM = "CPS:";
const char lcdString2[] PROGMEM = "CPM:";
const char lcdString3[] PROGMEM = "CPH:";
const char lcdString4[] PROGMEM = "CPS a:";
const char lcdString5[] PROGMEM = "CPM a:";
const char lcdString6[] PROGMEM = "CPH a:";
const char lcdString7[] PROGMEM = "Total:";

uint16_t adcAvg(uint8_t adcChannel, uint16_t counts){
    uint8_t tmpAdmux = ADMUX;
    uint32_t adcSum = 0;
    ADMUX = (1<<REFS0) | adcChannel;
    
    for(uint16_t i = 0; i<counts; i++){
        ADCSRA |= (1<<ADSC);
        while(ADCSRA & (1<<ADSC)){}
        adcSum += ADC;
    }
    ADMUX = tmpAdmux;
    return(adcSum / counts);
}

void calibrateAdc(){
    adcRefVoltage = (1.1 * ADC_RES) / adcAvg(14, AVG_CNT); 
    voltsPerDigit = (resistor1 / resistor2) * (adcRefVoltage / ADC_RES); 
}

void delayMs(uint16_t ms){
    for(; ms > 0; ms--){
         _delay_ms(1);
    }
}

uint32_t getMs(){ //returns the total number of milliseconds since power-on/reset     
    return(TCNT0 * CLOCK_TIME + ovf * CLOCK_TIME * 255); 
}

void showLineP(uint8_t xStart, uint8_t yStart, const char *firstString, const char *secondString){
    lcd_gotoxy(xStart, yStart);
    for(int i = 0; i<LCD_DISP_LENGTH; i++){
        lcd_putc(' ');
    }
    lcd_gotoxy(xStart, yStart);
    lcd_puts_p(firstString);
    lcd_putc(' ');
    lcd_puts(secondString);
}

void uartPutN(char *data, uint8_t size){
    for(uint8_t i = 0; i<size; i++){
        uart_putc(data[i]);
    }
}

uint16_t uartReadBuffer(unsigned char *charBuffer, uint16_t maxSize){
    uint16_t uartData;
    uint8_t byteCounter = 0;
    
    while(byteCounter < maxSize){
        uartData = uart_getc();
        
        if(uartData & UART_NO_DATA){
            break;
        }
        if(uartData & UART_BUFFER_OVERFLOW){
            uart_putc(UART_ERROR_OVERFLOW);
            continue;
        }
        if(uartData & UART_OVERRUN_ERROR){
            uart_putc(UART_ERROR_OVERRUN);
            continue;
        }
        if(uartData & UART_FRAME_ERROR){
            uart_putc(UART_ERROR_FRAME);
            continue;
        }

        charBuffer[byteCounter++] = uartData & 0xff; 
    }
    return(byteCounter);
}

void uartParseCmd(){
    static unsigned char uartBuffer[UART_RX_BUFFER_SIZE] = {'a'};
    static uint16_t uartBytesAvail = 0;
    
    uartBytesAvail += uartReadBuffer(uartBuffer + uartBytesAvail, UART_RX_BUFFER_SIZE - uartBytesAvail);
        
    if(uartBytesAvail > 2 && uartBuffer[uartBytesAvail - 1] == '\n'){
        switch(uartBuffer[0]){
            case UART_GET_PGAIN:{
                uart_putc(UART_VALID_RETURN);
                uart_putc((char)propGain);
                uart_puts("\n");
                break;
            }
            case UART_GET_IGAIN:{
                uart_putc(UART_VALID_RETURN);
                uart_putc((char)intGain);
                uart_puts("\n");
                break;
            }
            case UART_GET_OUTPUT_VOLTAGE:{
                char returnBuffer[sizeof(currVoltage)];
                memcpy(returnBuffer, &currVoltage, sizeof(currVoltage));
                uart_putc(UART_VALID_RETURN);
                uartPutN(returnBuffer, sizeof(returnBuffer));
                uart_puts("\n");
                break;
            }
            case UART_GET_TARGET_VOLTAGE:{
                char returnBuffer[sizeof(setPoint)];
                memcpy(returnBuffer, &setPoint, sizeof(returnBuffer));
                uart_putc(UART_VALID_RETURN);
                uartPutN(returnBuffer, sizeof(returnBuffer));
                uart_puts("\n");
                break;
            }
            case UART_GET_CYCLE_TIME:{
                char returnBuffer[sizeof(loopTime)];
                memcpy(returnBuffer, &loopTime, sizeof(returnBuffer));
                uart_putc(UART_VALID_RETURN);
                uartPutN(returnBuffer, sizeof(returnBuffer));                
                uart_puts("\n");
                break;
            }
            case UART_GET_TIME:{
                uint32_t time = getMs();
                char returnBuffer[sizeof(time)];
                memcpy(returnBuffer, &time, sizeof(returnBuffer));
                uart_putc(UART_VALID_RETURN);
                uartPutN(returnBuffer, sizeof(returnBuffer));                
                uart_puts("\n");
                break;
            }
            case UART_GET_UART_BAUD_RATE:{
                uint16_t baud = UART_BAUD_RATE;
                char returnBuffer[sizeof(baud)];
                memcpy(returnBuffer, &baud, sizeof(returnBuffer));
                uart_putc(UART_VALID_RETURN);
                uartPutN(returnBuffer, sizeof(returnBuffer));                
                uart_puts("\n");
                break;
            }
            case UART_GET_REF_VOLTAGE:{
                char returnBuffer[sizeof(adcRefVoltage)];
                memcpy(returnBuffer, (const void*)&adcRefVoltage, sizeof(adcRefVoltage));
                uart_putc(UART_VALID_RETURN);
                uartPutN(returnBuffer, sizeof(returnBuffer));                
                uart_puts("\n");
                break;
            }
            case UART_GET_RESISTOR_1_VALUE:{
                float resistance = resistor1;
                char returnBuffer[sizeof(resistance)];
                memcpy(returnBuffer, &resistance, sizeof(returnBuffer));
                uart_putc(UART_VALID_RETURN);
                uartPutN(returnBuffer, sizeof(returnBuffer));                
                uart_puts("\n");
                break;
            }
            case UART_GET_RESISTOR_2_VALUE:{
                float resistance = resistor2;
                char returnBuffer[sizeof(resistance)];
                memcpy(returnBuffer, &resistance, sizeof(returnBuffer));
                uart_putc(UART_VALID_RETURN);
                uartPutN(returnBuffer, sizeof(returnBuffer));                
                uart_puts("\n");
                break;
            }
            case UART_GET_ADC_AVG_COUNTS:{
                uint16_t counts = AVG_CNT;
                char returnBuffer[sizeof(counts)];
                memcpy(returnBuffer, &counts, sizeof(returnBuffer));
                uart_putc(UART_VALID_RETURN);
                uartPutN(returnBuffer, sizeof(returnBuffer));                
                uart_puts("\n");
                break;
            }
            case UART_GET_TOTAL_COUNTS:{
                char returnBuffer[sizeof(ticks)];
                memcpy(returnBuffer, (const void*)&ticks, sizeof(returnBuffer));
                uart_putc(UART_VALID_RETURN);
                uartPutN(returnBuffer, sizeof(returnBuffer));                
                uart_puts("\n");
                break;
            }
            
            case UART_SET_PGAIN:{
                /*we need two bytes for the instruction, 
                one for the data and one for line break, hence four bytes total*/                    
                if(uartBytesAvail < 4 || uartBuffer[1] != 1){ 
                    uart_putc(UART_INVALID_INST);
                    break;
                }
                propGain = uartBuffer[2];
                cli();
                eeprom_update_byte((uint8_t*)&propGainEE, propGain);
                sei();
                uart_putc(UART_SUCCESSFULL);
                break;
            }
            case UART_SET_IGAIN:{
                /*we need two bytes for the instruction, 
                one for the data and one for line break, hence four bytes total*/                    
                if(uartBytesAvail < 4 || uartBuffer[1] != 1){ 
                    uart_putc(UART_INVALID_INST);
                    break;
                }
                intGain = uartBuffer[2];
                cli();
                eeprom_update_byte((uint8_t*)&intGainEE, intGain);
                sei();
                uart_putc(UART_SUCCESSFULL);
                break;
            }
            case UART_SET_TARGET_VOLTAGE:{
                /*we need two bytes for the instruction, 
                two for the data and one for line break, hence five bytes total*/                    
                if(uartBytesAvail < 5 || uartBuffer[1] != sizeof(currVoltage)){ 
                    uart_putc(UART_INVALID_INST);
                    break;
                }
                uint8_t inputBuffer[sizeof(currVoltage)] = {uartBuffer[2], uartBuffer[3]};
                memcpy(&setPoint, inputBuffer, sizeof(inputBuffer));
                cli();
                eeprom_update_byte((uint8_t*)&setPointEE, setPoint);
                sei();
                uart_putc(UART_SUCCESSFULL);
                break;
            }
            case UART_SET_RESISTOR_1_VALUE:{
                /*we need two bytes for the instruction, 
                four for the data and one for line break, hence seven bytes total*/                    
                if(uartBytesAvail < 7 || uartBuffer[1] != sizeof(float)){ 
                    uart_putc(UART_INVALID_INST);
                    break;
                }
                uint8_t inputBuffer[sizeof(resistor1)] = {uartBuffer[2], uartBuffer[3], uartBuffer[4], uartBuffer[5]};
                memcpy(&resistor1, inputBuffer, sizeof(inputBuffer));
                cli();
                eeprom_update_float((float*)&resistor1EE, resistor1);
                sei();
                uart_putc(UART_SUCCESSFULL);
                break;
            }
            case UART_SET_RESISTOR_2_VALUE:{
                /*we need two bytes for the instruction, 
                four for the data and one for line break, hence seven bytes total*/                    
                if(uartBytesAvail < 7 || uartBuffer[1] != sizeof(float)){ 
                    uart_putc(UART_INVALID_INST);
                    break;
                }
                uint8_t inputBuffer[sizeof(resistor2)] = {uartBuffer[2], uartBuffer[3], uartBuffer[4], uartBuffer[5]};
                memcpy(&resistor2, inputBuffer, sizeof(inputBuffer));
                cli();
                eeprom_update_float((float*)&resistor2EE, resistor2);
                sei();
                uart_putc(UART_SUCCESSFULL);
                break;
            }
            default:{
                uart_putc(UART_INVALID_INST);
                break;
            }  
        }
        uartBytesAvail = 0;
    }else if(uartBytesAvail == UART_RX_BUFFER_SIZE && uartBuffer[uartBytesAvail - 1] != '\n'){
        uart_putc(UART_INVALID_INST);
        uartBytesAvail = 0;
    }
}

ISR(INT0_vect){
    ticks++;
    ctrlBools0 |= (1<<TICK_CHANGE);
    LED_PORT |= (1<<LED_PIN);
    TCCR2B = TIMER2_REGISTER;
}

ISR(TIMER0_OVF_vect){
    ovf++;
}

ISR(TIMER2_COMPA_vect){
    TCCR2B = 0;
    TCNT2 = 0;
    LED_PORT &= ~(1<<LED_PIN);
}

int main(void) {
    
    cli();

    /*Port setup*/
    DDRB |= (1<<PB1);
    DDRC |= (1<<PC5);
    DDRD |= (1<<PD4);
    PORTD |= (1<<PD2) | (1<<PD3);
    
    /*External interrupt setup*/
    EICRA |= (1<<ISC01); //setting up INT0 to trigger on falling edges
    EIMSK |= (1<<INT0); //enabling INT0
    
    /*Timer 0 setup (timing)*/
    TCCR0B = 0;
    TCNT0 = 0;
    TIMSK0 = (1<<TOIE0);
    TCCR0B |= (1<<CS02) | (1<<CS00);
    
    /*Timer 1 setup (for PWM)*/
    TCCR1A = (1<<COM1A1) | (0<<COM1A0) | (1<<WGM10) | (1<<WGM11);
    OCR1A = 0;
    TCCR1B = (0<<CS10) | (1<<CS11);
        
    /*Timer 2 setup for LED timing*/
    TCNT2 = 0;
    OCR2A = 156;
    TIMSK2 = (1<<OCIE2A);
    TCCR2B = TIMER2_REGISTER;
    
    /*ADC setup*/
    ADMUX = (1<<REFS0);
    ADCSRB =  0;
    ADCSRA = (1<<ADPS2) | (1<<ADPS0) | (1<<ADEN);
    
    calibrateAdc();
        
    propGain = eeprom_read_byte((uint8_t*)&propGainEE);
    intGain = eeprom_read_byte((uint8_t*)&intGainEE);
    setPoint = eeprom_read_word((uint16_t*)&setPointEE);
    resistor1 = eeprom_read_float((float*)&resistor1EE);
    resistor2 = eeprom_read_float((float*)&resistor2EE);
    
    if(propGain == 0xff){
        eeprom_update_byte((uint8_t*)&propGainEE, PROP_GAIN);
        eeprom_update_byte((uint8_t*)&intGainEE, INT_GAIN);
        eeprom_update_word((uint16_t*)&setPointEE, SET_POINT);
        eeprom_update_float((float*)&resistor1EE, RESISTOR_R1);
        eeprom_update_float((float*)&resistor2EE, RESISTOR_R2);
    }
    
    uart_init(UART_BAUD_SELECT(UART_BAUD_RATE, F_CPU));
    
    sei();
    
    /*Initializig variables */
    float error = 0; //the controller error
    float integral = 0; //the result of the integral controller
    float tmpIntegral = 0; //stores the last result of the integral controller that doesn't cause overshoot
    int16_t output = 0; //the controller output
    int16_t oldVoltage = 0; 
    //uint32_t adcSum; //adds multiple results from the adc to calculate an average
    float oldLoopTime = 0; //used to calculate the cycle time
    uint32_t totalTime = 0; //stores the result from getMs() at the start of the loop
    uint32_t oldSecondTime = 0; //these are used to calculate the rates
    uint32_t oldSecondTicks = 0;
    uint32_t secondTime = 0;
    uint32_t intervalTime = 0;
    uint32_t oldIntervalTime = 0;
    uint32_t oldIntervalTicks = 0;
    
    /*LCD init*/
    char lcdStringBuffer[12] = "";
    char currVoltString[8] = "";
    char refVoltString[8] = "";
    lcd_init(LCD_DISP_ON);
    lcd_clrscr();
    lcd_puts_p(lcdString0);
    delayMs(1000);
    lcd_clrscr();
    ctrlBools0 |= (1<<MENU_CHANGE);
    
    while(1){
        PORTC ^= (1<<PC5);
        totalTime = getMs();
        secondTime = totalTime - oldSecondTime;
        intervalTime = totalTime - oldIntervalTime;

        if(secondTime > 1000){ //every second
            ctrlBools0 |= (1<<VOLTAGE_CHANGE) | (1<<RATE_CHANGE);
            ticksPerSecond = (ticks - oldSecondTicks) / ((float)secondTime / 1000);
            ticksPerSecondAvg = ticks / ((float)totalTime / 1000);
            oldSecondTicks = ticks;
            oldSecondTime = totalTime;
            uartParseCmd();
        }
        if(intervalTime > 20000){ //every 20 seconds
            ticksPerMinute = (ticks - oldIntervalTicks) / ((float)intervalTime / 60000);
            ticksPerMinuteAvg = ticks / ((float)totalTime / 60000);
            ticksPerHour = (ticks - oldIntervalTicks) / ((float)intervalTime / 3600000);
            ticksPerHourAvg = ticks / ((float)totalTime / 3600000);
            oldIntervalTime = totalTime;
            oldIntervalTicks = ticks;
            calibrateAdc();
        }
        /*Start of PI-Controller*/
        loopTime = totalTime / 1000 - oldLoopTime;
        oldLoopTime = totalTime / 1000;   
        
        currVoltage = adcAvg(0, AVG_CNT) * voltsPerDigit;
        error = setPoint - currVoltage;
	integral += error * loopTime;
                
        output = round((error * propGain) + (intGain * integral));
                
	if(output < 0){
            output = 0;
            integral = tmpIntegral;
	}else if(output > 1023){
            output = 1023;
            integral = tmpIntegral;             
	}else{
            tmpIntegral = integral;
        }
                             
	OCR1A = output;
        /*End of PI-Controller*/
        
        if(!(PIND & (1<<PD3))){
            ctrlBools0 |= (1<<BTN_PRESSED);
        }
        
        if((ctrlBools0 & (1<<BTN_PRESSED) && (PIND & (1<<PD3)))){
            if(menuState == 7){
                menuState = 1;
            }else{
                menuState++;
            }
            ctrlBools0 &= ~(1<<BTN_PRESSED);
            ctrlBools0 |= (1<<MENU_CHANGE);
        }
        
        if((ctrlBools0 & (1<<RATE_CHANGE)) || (ctrlBools0 & (1<<MENU_CHANGE))){
            switch(menuState){
                case 1:
                    dtostrf(ticksPerSecond, 2, 1, lcdStringBuffer);
                    showLineP(0, 0, lcdString1, lcdStringBuffer);
                    break;
                case 2:
                    dtostrf(ticksPerMinute, 2, 1, lcdStringBuffer);
                    showLineP(0, 0, lcdString2, lcdStringBuffer);
                    break;
                case 3:
                    dtostrf(ticksPerHour, 2, 1, lcdStringBuffer);
                    showLineP(0, 0, lcdString3, lcdStringBuffer);
                    break;
                case 4:
                    dtostrf(ticksPerSecondAvg, 2, 1, lcdStringBuffer);
                    showLineP(0, 0, lcdString4, lcdStringBuffer);
                    break;
                case 5:
                    dtostrf(ticksPerMinuteAvg, 2, 1, lcdStringBuffer);
                    showLineP(0, 0, lcdString5, lcdStringBuffer);
                    break;
                case 6:
                    dtostrf(ticksPerHourAvg, 2, 1, lcdStringBuffer);
                    showLineP(0, 0, lcdString6, lcdStringBuffer);
                    break;
                case 7:
                    ltoa(ticks, lcdStringBuffer, 10);
                    showLineP(0, 0, lcdString7, lcdStringBuffer);
                    break;
            }
            ctrlBools0 &= ~(1<<MENU_CHANGE) & ~(1<<RATE_CHANGE);
        }
        
        if((ctrlBools0 & (1<<VOLTAGE_CHANGE) && (oldVoltage != currVoltage))){
            dtostrf(adcRefVoltage, 2, 2, refVoltString);
            itoa(currVoltage, currVoltString, 10);
            lcd_gotoxy(0, 1);
            lcd_puts(refVoltString);
            lcd_puts("V ");
            lcd_puts(currVoltString);
            lcd_puts("V");
            ctrlBools0 &= ~(1<<VOLTAGE_CHANGE);
            oldVoltage = currVoltage;
        }
        if(!(ctrlBools0 & (1<<ADC_CALIBRATED))){
            calibrateAdc();
            ctrlBools0 |= (1<<ADC_CALIBRATED);
        }
    }   
}
